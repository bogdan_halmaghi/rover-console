﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rover
{
    class Enemy
    {

        public Enemy(int xR,int yR,ref char[,] tabla, ref int x, ref int y)
        {
            bool shit=true;
            while (shit)
            {
                Random pozitieRover = new Random();
                x = pozitieRover.Next(0, 21);
                y = pozitieRover.Next(0, 44);
                Random rnd = new Random();
                if (tabla[x, y] == '≡')
                {
                    tabla[x, y] = State(rnd.Next(1, 4));
                    shit = false;
                }
            } 
        }
        private char State(int state)
        {

            switch (state)
            {
                case 1: return '▲';
                case 2: return '▼';
                case 3: return '►';
                case 4: return '◄';
                default: return 'f';
            }
        }
        public void GoAfter(int xR, int yR, ref char[,] tab, ref int xA, ref int yA)
        {
            Random rnd = new Random();
            if (rnd.Next(0, 10) < 5)
                Xmove(ref tab,xR, ref xA,ref yA);
            else
                Ymove(ref tab,yR, ref xA,ref yA);
        }
        private void Xmove(ref char[,] tabla,int xR, ref int xA,ref int yA)
        {
            
            if (xR < xA)
            {
                tabla[xA, yA] = '≡';
                xA--;
                tabla[xA, yA] = '▲';
            }
            else if (xR != xA)
            {
                tabla[xA, yA] = '≡';
                xA++;
                tabla[xA, yA] = '▼';
            }
        }
        private void Ymove(ref char[,] tabla, int yR, ref int xA, ref int yA)
        {
            
            if (yR < yA)
            {
                tabla[xA, yA] = '≡';
                yA--;
                tabla[xA, yA] = '◄';
            }
            else if (yR != yA)
            {
                tabla[xA, yA] = '≡';
                yA++;
                tabla[xA, yA] = '►';
            }

        }
    }
}