﻿//Good Luck! (y) 




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rover
{
    class Program
    {
        static void Main(string[] args)
        {

            string matriceAfis = String.Empty;
            int  xR = 0, yR = 0,xA1=0,yA1=0,xA2=0,yA2=0,val=-5;
            char[,] table = new char[22, 45];

            Table.Initializare(table);

            Rover R1 = new Rover(ref table, ref xR, ref  yR);
            Enemy E1 = new Enemy(xR, yR, ref table, ref xA1, ref yA1);
            Enemy E2 = new Enemy(xR, yR, ref table, ref xA2, ref yA2);
            Table.afisare(table, ref matriceAfis, ref val);

           
            do
            {

                R1.Move(ref table, ref xR, ref yR);
                E1.GoAfter(xR, yR, ref table, ref xA1, ref yA1);
                E2.GoAfter(xR, yR, ref table, ref xA2, ref yA2);
                Console.Clear();

                Table.afisare(table, ref matriceAfis,ref val);
                
                stop(xR, yR, xA1, yA1,xA2,yA2,val);
                
            } while (1 == 1);




        }

      

        private static void stop(int xR, int yR, int xA1, int yA1,int xA2,int yA2,int val)
        {
            if (xR == xA1 && yR == yA1 || xR == xA2 && yR == yA2)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("\n\n\n\nGame Over! ");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\n\n Better Luck next time!\n ");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("You achieved {0} points!",val);
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadKey();
                Environment.Exit(1);
               
            }
        }
    }
}
