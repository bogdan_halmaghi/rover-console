﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rover
{

    class Rover
    {
        public Rover(ref char[,] tabla, ref int x, ref int y)
        {
            Random pozitieRover=new Random();
            x=pozitieRover.Next(0,21);
            y=pozitieRover.Next(0,44);
            Random rnd = new Random();
            tabla[x, y] = State(rnd.Next(1, 4));

        }

        private char State(int state)
        {

            switch (state)
            {
                case 1: return '↑';
                case 2: return '↓';
                case 3: return '→';
                case 4: return '←';
                default: return 'f';
            }
        }

        internal void Move(ref char[,] tabla, ref int x, ref int y)
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey(true);
//Functie booleana la if-uri . Pui acolo toate obstacolele (eventual random :-?...
            switch (keyInfo.Key)
            {
                case ConsoleKey.UpArrow:
                    if (x != 0 &&tabla[x,y]!=tabla[16,30])
                    {
                        tabla[x, y] = '≡';
                        x -= 1;
                        tabla[x, y] = '↑';
                    }
                    
                    break;
                case ConsoleKey.DownArrow:
                    if (x != 21 && tabla[x, y] != tabla[14, 30])
                    {
                        tabla[x, y] = '≡';
                        x += 1;
                        tabla[x, y] = '↓';
                    }
                    break;
                case ConsoleKey.RightArrow:
                    if (y != 44 && tabla[x, y] != tabla[15, 29])
                    {
                        tabla[x, y] = '≡';
                        y += 1;
                        tabla[x, y] = '→';
                    }
                    break;
                case ConsoleKey.LeftArrow:
                    if (y != 0 && tabla[x, y] != tabla[15, 31])
                    {
                        tabla[x, y] = '≡';
                        y -= 1;
                        tabla[x, y] = '←';
                    }
                    break;
                case ConsoleKey.Escape:
                    Environment.Exit(0);
                    break;

            }
        }

    }
}
       



    





